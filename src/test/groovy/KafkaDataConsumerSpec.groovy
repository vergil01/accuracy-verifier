

import com.google.gson.Gson
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.TopicPartition
import spock.lang.Specification
import uk.ac.leedsBeckett.model.Consumption
import uk.ac.leedsBeckett.verification.KafkaDataConsumer

import java.time.Duration
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.stream.Collectors

/**
 * Created by vergil01 on 13/10/2017.
 */
class KafkaDataConsumerSpec extends Specification {
    Consumption consumption1
    Consumption consumption2
    Consumption consumption3
    Consumption consumption4
    Consumption consumption5
    Duration oneSecond
    Duration twoSeconds
    String topic
    Gson gson
    List<ConsumerRecord<String, String>> recordList
    TopicPartition topicPartition

    def setup() {
        oneSecond = Duration.ofSeconds(1)
        twoSeconds = Duration.ofSeconds(2)
        consumption1 = new Consumption(10, oneSecond)
        consumption2 = new Consumption(9, oneSecond)
        consumption3 = new Consumption(8, oneSecond)
        consumption4 = new Consumption(9, oneSecond)
        consumption5 = new Consumption(10, oneSecond)
        topic = "dummyTopic"
        gson = new Gson()
        recordList = Arrays.asList(consumption1, consumption2, consumption3, consumption4, consumption5)
                .stream()
                .map { c -> new ConsumerRecord<>("dummyTopic", 1, 0, gson.toJson(ZonedDateTime.now(ZoneId.of("UTC"))), gson.toJson(c)) }
                .collect(Collectors.toList())
        topicPartition = new TopicPartition(topic, 1)
    }

    def "test that readFromTopic() applies a function to each value returned"() {

        given: 'A mock Kafka Consumer that returns a list of Consumer Records and checks that the subscribe() method is called with the right topic.'
        ConsumerRecords<String, String> consumerRecords = new ConsumerRecords<>([topicPartition: recordList])
        KafkaConsumer<String, String> kafkaConsumer = Mock(KafkaConsumer) {
            1 * subscribe(Arrays.asList(topic))
            poll(1000) >>> [consumerRecords, null, null]
        }

        and: "A spy instance of KafkaDataConsumer where external collaborator (the Kafka Consumer) is mocked"
        KafkaDataConsumer kafkaDataConsumer = Spy(KafkaDataConsumer)
        kafkaDataConsumer.consumer = kafkaConsumer

        and: "A List to store the returned values"
        List<ConsumerRecord<String, String>> returnedValues = []

        when: 'The readFromTopic() method is called, polling every second and timing out after 2 seconds.'
        kafkaDataConsumer.readFromTopic(topic, oneSecond, { r -> returnedValues.add(r) }, twoSeconds)

        then: 'The returned values are correct, and no exception is thrown.'
        returnedValues.size() == recordList.size()
        returnedValues.containsAll(recordList)
        notThrown(Exception)

        and: 'The Kafka Consumer is closed.'
        1 * kafkaConsumer.close()
    }

    def "test that readFromTopic() continues to poll if the first poll returns nothing"() {

        given:
        'A mock Kafka Consumer that returns a list of Consumer Records the second time it is invoked, ' +
                'and checks that the subscribe() method is called with the right topic.'
        ConsumerRecords<String, String> consumerRecords = new ConsumerRecords<>([topicPartition: recordList])
        KafkaConsumer<String, String> kafkaConsumer = Mock(KafkaConsumer) {
            1 * subscribe(Arrays.asList(topic))
            poll(1000) >>> [null, consumerRecords, null]
        }

        and: "A spy instance of KafkaDataConsumer where external collaborator (the Kafka Consumer) is mocked"
        KafkaDataConsumer kafkaDataConsumer = Spy(KafkaDataConsumer)
        kafkaDataConsumer.consumer = kafkaConsumer

        and: "A List to store the returned values"
        List<ConsumerRecord<String, String>> returnedValues = []

        when: 'The readFromTopic() method is called, polling every second and timing out after 2 seconds.'
        kafkaDataConsumer.readFromTopic(topic, oneSecond, { r -> returnedValues.add(r) }, twoSeconds)

        then: 'The returned values are correct, and no exception is thrown.'
        returnedValues.size() == recordList.size()
        returnedValues.containsAll(recordList)
        notThrown(Exception)

        and: 'The Kafka Consumer is closed.'
        1 * kafkaConsumer.close()
    }

    def "test that readFromTopic() closes the Kafka consumer and terminates as expected when no records are received"() {

        given: 'A mock Kafka Consumer that checks that the subscribe() method is called with the right topic and returns no records.'
        KafkaConsumer<String, String> kafkaConsumer = Mock(KafkaConsumer) {
            1 * subscribe(Arrays.asList(topic))
            poll(1000) >>> [null, null, null]
        }

        and: "A spy instance of KafkaDataConsumer where external collaborator (the Kafka Consumer) is mocked"
        KafkaDataConsumer kafkaDataConsumer = Spy(KafkaDataConsumer)
        kafkaDataConsumer.consumer = kafkaConsumer

        and: "A List to store the returned values"
        List<ConsumerRecord<String, String>> returnedValues = []

        when: 'The readFromTopic() method is called, polling every second and timing out after 2 seconds.'
        kafkaDataConsumer.readFromTopic(topic, oneSecond, { r -> returnedValues.add(r) }, twoSeconds)

        then: 'The function that populates the returned values list is never called, the list is empty, and no exception is thrown.'
        returnedValues.isEmpty()
        notThrown(Exception)

        and: 'The Kafka Consumer is closed.'
        1 * kafkaConsumer.close()
    }

}
