package uk.ac.leedsBeckett.verification;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by vergil01 on 20/10/2017.
 */
public class RunKafkaConsumer {

    public static void main(String[] args) {
        ExecutorService service = Executors.newCachedThreadPool();
        Arrays.asList("realEnergy", "itEnergy", "results")
//        Arrays.asList("results")
                .forEach(topic ->
                        service.submit(() -> {
                            try {
                                new KafkaDataConsumer().readFromTopic(topic, Duration.ofMillis(500), x -> {
                                    System.out.println("topic: " + x.topic());
                                    System.out.println("key: " + x.key());
                                    System.out.println("value: " + x.value());
                                    return x;
                                }, Duration.ofMinutes(30));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        })
                );

    }
}
