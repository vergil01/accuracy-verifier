package uk.ac.leedsBeckett.verification;

import com.google.common.io.Resources;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;
import java.util.function.Function;

/**
 * Created by vergil01 on 06/10/2017.
 * This class creates a simple consumer that reads data from a Kafka topic.
 */
public class KafkaDataConsumer {

    protected KafkaConsumer<String, String> consumer;

    /**
     * This method reads data from a specified topic and applies a function to each record retrieved.
     *
     * @param topic            The Kafka topic to read from.
     * @param pollingFrequency The frequency at which the Kafka topic is polled for new records.
     * @param function         The function to apply to each record.
     * @param runningTime      The duration after which the consumer will time out and close.
     * @throws IOException
     */
    public void readFromTopic(String topic, Duration pollingFrequency, Function<ConsumerRecord<String, String>, ?> function, Duration runningTime) throws IOException {

        if (consumer == null) {
            initialiseConsumer();
        }
        consumer.subscribe(Arrays.asList(topic));
        consumer.poll(0);
        consumer.seekToBeginning(consumer.assignment().toArray(new TopicPartition[consumer.assignment().size()]));

        Instant now = Instant.now();
        Instant timeout = now.plus(runningTime);

        while (now.isBefore(timeout)) {
            ConsumerRecords<String, String> records = consumer.poll(pollingFrequency.toMillis());
            if (records != null && !records.isEmpty()) {
                for (ConsumerRecord<String, String> record : records) {
                    function.apply(record);
                }
            }
            now = Instant.now();
        }
        consumer.close();
    }

    private void initialiseConsumer() throws IOException {
        try (InputStream props = Resources.getResource("consumer.props").openStream()) {
            Properties properties = new Properties();
            properties.load(props);
            if (properties.getProperty("group.id") == null) {
                properties.setProperty("group.id", "group-" + UUID.randomUUID().toString());
            }
            if (properties.getProperty("consumer.id") == null) {
                properties.setProperty("consumer.id", "consumer-" + UUID.randomUUID().toString());
            }
            consumer = new KafkaConsumer<>(properties);
        }
    }

}
